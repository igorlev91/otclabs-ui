import Vue from 'vue'
import VueRouter from 'vue-router'

import HomePage from '../views/HomePage'
import Signin from '../views/Signin'
import Register from '../views/Register'
import Dashbord from '../views/Dashbord'
import Wallet from '../views/Wallet'
import TransferFunds from '../views/TransferFunds'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomePage
  
  },
  {
    path: '/signin',
    name: 'signin',
    component: Signin
  
  },
  {
    path: '/register',
    name: 'register',
    component: Register
  
  },
  {
    path: '/dashbord',
    name: 'dashbord',
    component: Dashbord
  
  },

  {
    path: '/wallet',
    name: 'wallet',
    component: Wallet
  },
  {
    path: '/funds',
    name: 'funds',
    component: TransferFunds
  },

  // {  
  //   path: '**',
  //   name: 'any',
  //   component: Signin
  // }

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
