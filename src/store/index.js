import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    //responsible for switching tables in wallet page
    buttonActiveClass : false,

    //responsible for steps in the register form component
    isStepOneOfRegisterDone : false,
    isStepTwoOfRegisterDone : false,

  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
